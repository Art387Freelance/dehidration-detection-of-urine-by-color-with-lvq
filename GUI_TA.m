function varargout = GUI_TA(varargin)
% GUI_TA MATLAB code for GUI_TA.fig
%      GUI_TA, by itself, creates a new GUI_TA or raises the existing
%      singleton*.
%
%      H = GUI_TA returns the handle to a new GUI_TA or the handle to
%      the existing singleton*.
%
%      GUI_TA('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_TA.M with the given input arguments.
%
%      GUI_TA('Property','Value',...) creates a new GUI_TA or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_TA_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_TA_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI_TA

% Last Modified by GUIDE v2.5 14-Jul-2019 20:22:24

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @GUI_TA_OpeningFcn, ...
                   'gui_OutputFcn',  @GUI_TA_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GUI_TA is made visible.
function GUI_TA_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI_TA (see VARARGIN)

% Choose default command line output for GUI_TA
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);


load data_latih.mat

if strcmp(jenis_layer,'RGB')
    set(handles.text2,'String','R Mean');
    set(handles.text3,'String','G Mean');
    set(handles.text4,'String','B Mean');
    set(handles.text5,'String','R Max');
    set(handles.text6,'String','G Max');
    set(handles.text7,'String','B Max');
    set(handles.text8,'String','R Min');
    set(handles.text9,'String','G Min');
    set(handles.text10,'String','B Min');
elseif strcmp(jenis_layer,'HSV')
    set(handles.text2,'String','H Mean');
    set(handles.text3,'String','S Mean');
    set(handles.text4,'String','V Mean');
    set(handles.text5,'String','H Max');
    set(handles.text6,'String','S Max');
    set(handles.text7,'String','V Max');
    set(handles.text8,'String','H Min');
    set(handles.text9,'String','S Min');
    set(handles.text10,'String','V Min');
    
elseif strcmp(jenis_layer,'YCBCR')
    set(handles.text2,'String','Y Mean');
    set(handles.text3,'String','Cb Mean');
    set(handles.text4,'String','Cr Mean');
    set(handles.text5,'String','Y Max');
    set(handles.text6,'String','Cb Max');
    set(handles.text7,'String','Cr Max');
    set(handles.text8,'String','Y Min');
    set(handles.text9,'String','Cb Min');
    set(handles.text10,'String','Cr Min');
    
elseif strcmp(jenis_layer,'LAB')
    set(handles.text2,'String','L Mean');
    set(handles.text3,'String','A Mean');
    set(handles.text4,'String','B Mean');
    set(handles.text5,'String','L Max');
    set(handles.text6,'String','A Max');
    set(handles.text7,'String','B Max');
    set(handles.text8,'String','L Min');
    set(handles.text9,'String','A Min');
    set(handles.text10,'String','B Min');
end
% UIWAIT makes GUI_TA wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = GUI_TA_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double


% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double


% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit10_Callback(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit10 as text
%        str2double(get(hObject,'String')) returns contents of edit10 as a double


% --- Executes during object creation, after setting all properties.
function edit10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editBrowse_Callback(hObject, eventdata, handles)
% hObject    handle to editBrowse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editBrowse as text
%        str2double(get(hObject,'String')) returns contents of editBrowse as a double


% --- Executes during object creation, after setting all properties.
function editBrowse_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editBrowse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in browse.
function browse_Callback(hObject, eventdata, handles)
% hObject    handle to browse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global img
[filename,pathname] = uigetfile('*.jpg');
bacaimage = fullfile(pathname,filename);
img = imread(bacaimage);
axes(handles.axes1);
imshow(img);
set(handles.editBrowse,'string',filename);


% --- Executes on button press in preproc.
function preproc_Callback(hObject, eventdata, handles)
% hObject    handle to preproc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img imgBw lab
load data_latih.mat
cform = makecform('srgb2lab');
lab = applycform(img,cform);

% proses segmentasi citra menggunakan k-mean clustering
ab = double(lab(:,:,2:3));
nrows = size(ab,1);
ncols = size(ab,2);
ab = reshape(ab,nrows*ncols,2);

nColors = 3;
[cluster_idx, cluster_center] = kmeans(ab,nColors,'distance','sqEuclidean','Replicates',3);
pixel_labels = reshape(cluster_idx,nrows,ncols);
RGB = label2rgb(pixel_labels);

%menampilkan hasil segmentasi pada masing-masing cluster
segmented_images = cell(1,3);
rgb_label = repmat(pixel_labels,[1 1 3]);

for k = 1:nColors
    color = img;
    color(rgb_label ~= k) = 0;
    segmented_images{k} = color;
end

%proses filling holes untuk menghilangkan noise
% Urin segmentation
area_cluster1 = sum(sum(pixel_labels==1));
area_cluster2 = sum(sum(pixel_labels==2));
area_cluster3 = sum(sum(pixel_labels==3));

[~,cluster_all] = min([area_cluster1,area_cluster2,area_cluster3]);
imgBw = (pixel_labels==cluster_all);
imgBw = imfill(imgBw,'holes');
imgBw = bwareaopen(imgBw,area_open);

axes(handles.axes2);
imshow(lab);

% --- Executes on button press in extraction.
function extraction_Callback(hObject, eventdata, handles)
% hObject    handle to extraction (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global imgBw lab ciri img

load data_latih.mat

% Shape features extraction
reg_stats = regionprops(imgBw,'Area','Perimeter','Eccentricity');

area = reg_stats.Area;
perimeter = reg_stats.Perimeter;
eccentricity = reg_stats.Eccentricity;
ciri_shape = [area perimeter eccentricity];

if strcmp(jenis_layer,'RGB')
    temp = img;
    R = temp(:,:,1);
    G = temp(:,:,2);
    B = temp(:,:,3);
    R(~imgBw) = 0;
    G(~imgBw) = 0;
    B(~imgBw) = 0;
    urinCat = cat(3,R,G,B);
    
    % menampilkan histogram pada masing-masing kanal RGB
    % RGB Features Extraction
    R_stats = regionprops(imgBw,R,'PixelValues','MeanIntensity',...
        'MaxIntensity','MinIntensity');
    G_stats = regionprops(imgBw,G,'PixelValues','MeanIntensity',...
        'MaxIntensity','MinIntensity');
    B_stats = regionprops(imgBw,B,'PixelValues','MeanIntensity',...
        'MaxIntensity','MinIntensity');
    
    R_pix_val = R_stats.PixelValues;
    G_pix_val = G_stats.PixelValues;
    B_pix_val = B_stats.PixelValues;
    
    R_mean = R_stats.MeanIntensity;
    G_mean = G_stats.MeanIntensity;
    B_mean = B_stats.MeanIntensity;
    
    R_max = R_stats.MaxIntensity;
    G_max = G_stats.MaxIntensity;
    B_max = B_stats.MaxIntensity;
    
    R_min = R_stats.MinIntensity;
    G_min = G_stats.MinIntensity;
    B_min = B_stats.MinIntensity;
    
    ciri = [R_mean G_mean B_mean R_max G_max B_max R_min G_min B_min];
elseif strcmp(jenis_layer,'HSV')
    % % HSV features extraction
    temp = rgb2hsv(img);
    H = temp(:,:,1);
    S = temp(:,:,2);
    V = temp(:,:,3);
    H(~imgBw) = 0;
    S(~imgBw) = 0;
    V(~imgBw) = 0;
    urinCat = cat(3,H,S,V);
    
    H_stats = regionprops(imgBw,H,'PixelValues','MeanIntensity',...
        'MaxIntensity','MinIntensity');
    S_stats = regionprops(imgBw,S,'PixelValues','MeanIntensity',...
        'MaxIntensity','MinIntensity');
    V_stats = regionprops(imgBw,V,'PixelValues','MeanIntensity',...
        'MaxIntensity','MinIntensity');
    
    H_mean = H_stats.MeanIntensity;
    S_mean = S_stats.MeanIntensity;
    V_mean = V_stats.MeanIntensity;
    
    H_max = H_stats.MaxIntensity;
    S_max = S_stats.MaxIntensity;
    V_max = V_stats.MaxIntensity;
    
    H_min = H_stats.MinIntensity;
    S_min = S_stats.MinIntensity;
    V_min = V_stats.MinIntensity;
    
    ciri = [H_mean S_mean V_mean H_max S_max V_max H_min S_min V_min];
    
elseif strcmp(jenis_layer,'YCBCR')
    temp = rgb2ycbcr(img);
    Y = temp(:,:,1);
    CB = temp(:,:,2);
    CR = temp(:,:,3);
    Y(~imgBw) = 0;
    CB(~imgBw) = 0;
    CR(~imgBw) = 0;
    urinCat = cat(3,Y,CB,CR);
    
    Y_stats = regionprops(imgBw,Y,'PixelValues','MeanIntensity',...
        'MaxIntensity','MinIntensity');
    CB_stats = regionprops(imgBw,CB,'PixelValues','MeanIntensity',...
        'MaxIntensity','MinIntensity');
    CR_stats = regionprops(imgBw,CR,'PixelValues','MeanIntensity',...
        'MaxIntensity','MinIntensity');
    
    Y_mean = Y_stats.MeanIntensity;
    CB_mean = CB_stats.MeanIntensity;
    CR_mean = CR_stats.MeanIntensity;
    
    Y_max = Y_stats.MaxIntensity;
    CB_max = CB_stats.MaxIntensity;
    CR_max = CR_stats.MaxIntensity;
    
    Y_min = Y_stats.MinIntensity;
    CB_min = CB_stats.MinIntensity;
    CR_min = CR_stats.MinIntensity;
    ciri = [Y_mean CB_mean CR_mean Y_max CB_max CR_max Y_min CB_min CR_min];
elseif strcmp(jenis_layer,'LAB')
    temp = lab;
    L = temp(:,:,1);
    A = temp(:,:,2);
    B = temp(:,:,3);
    L(~imgBw) = 0;
    A(~imgBw) = 0;
    B(~imgBw) = 0;
    urinCat = cat(3,L,A,B);
    
    L_stats = regionprops(imgBw,L,'PixelValues','MeanIntensity',...
        'MaxIntensity','MinIntensity');
    A_stats = regionprops(imgBw,A,'PixelValues','MeanIntensity',...
        'MaxIntensity','MinIntensity');
    B_stats = regionprops(imgBw,B,'PixelValues','MeanIntensity',...
        'MaxIntensity','MinIntensity');
    
    L_mean = L_stats.MeanIntensity;
    A_mean = A_stats.MeanIntensity;
    B_mean = B_stats.MeanIntensity;
    
    L_max = L_stats.MaxIntensity;
    A_max = A_stats.MaxIntensity;
    B_max = B_stats.MaxIntensity;
    
    L_min = L_stats.MinIntensity;
    A_min = A_stats.MinIntensity;
    B_min = B_stats.MinIntensity;
    ciri = [L_mean A_mean B_mean L_max A_max B_max L_min A_min B_min];
end
   set(handles.edit2,'String',ciri(1));
   set(handles.edit3,'String',ciri(2));
   set(handles.edit4,'String',ciri(3));
   set(handles.edit5,'String',ciri(4));
   set(handles.edit6,'String',ciri(5));
   set(handles.edit7,'String',ciri(6));
   set(handles.edit8,'String',ciri(7));
   set(handles.edit9,'String',ciri(8));
   set(handles.edit10,'String',ciri(9));




% --- Executes on button press in identifikasi.
function identifikasi_Callback(hObject, eventdata, handles)
% hObject    handle to identifikasi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global ciri
load data_latih.mat

for ii=1:size(ciri)
    ciri(ii) = ciri(ii)/ciriMax(ii);
end

y2 = net(ciri');
classes = vec2ind(y2);

if classes==1
    set(handles.text11,'string','Dehidrasi Berat');
elseif classes==2
    set(handles.text11,'string','Dehidrasi Rendah');
elseif classes==3
    set(handles.text11,'string','Dehidrasi Sedang');
end
