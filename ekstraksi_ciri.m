clc;
clear;
close all;
Img = imread('urinC15.jpg');
figure, imshow(Img), title('original image');

% melakukan segmentasi citra menggunakan metode k-mean clustering
% konversi terlebih dahulu ke dalam L*a*b color space
cform = makecform('srgb2lab');
lab = applycform(Img,cform);
figure, imshow(lab), title('L*a*b color space');

% proses segmentasi citra menggunakan k-mean clustering
ab = double(lab(:,:,2:3));
nrows = size(ab,1);
ncols = size(ab,2);
ab = reshape(ab,nrows*ncols,2);
 
nColors = 3;
[cluster_idx, cluster_center] = kmeans(ab,nColors,'distance','sqEuclidean','Replicates',3);
pixel_labels = reshape(cluster_idx,nrows,ncols);
RGB = label2rgb(pixel_labels);
figure, imshow(RGB,[]), title('image labeled by cluster index');

%menampilkan hasil segmentasi pada masing-masing cluster
segmented_images = cell(1,3);
rgb_label = repmat(pixel_labels,[1 1 3]);
 
for k = 1:nColors
    color = Img;
    color(rgb_label ~= k) = 0;
    segmented_images{k} = color;
end
 
figure,imshow(segmented_images{1}), title('objects in cluster 1');
figure,imshow(segmented_images{2}), title('objects in cluster 2');
figure,imshow(segmented_images{3}), title('objects in cluster 3');

%proses filling holes untuk menghilangkan noise
% Urin segmentation
area_cluster1 = sum(sum(pixel_labels==1));
area_cluster2 = sum(sum(pixel_labels==2));
area_cluster3 = sum(sum(pixel_labels==3));
 
[~,cluster_urinC15] = min([area_cluster1,area_cluster2,area_cluster3]);
urinC15_bw = (pixel_labels==cluster_urinC15);
urinC15_bw = imfill(urinC15_bw,'holes');
urinC15_bw = bwareaopen(urinC15_bw,1000);
 
urinC15 = Img;
R = urinC15(:,:,1);
G = urinC15(:,:,2);
B = urinC15(:,:,3);
R(~urinC15_bw) = 0;
G(~urinC15_bw) = 0;
B(~urinC15_bw) = 0;
urinC15_rgb = cat(3,R,G,B);
figure, imshow(urinC15_rgb), title('the urinC15 only (RGB Color Space)');

% menampilkan histogram pada masing-masing kanal RGB
% RGB Features Extraction
R_stats = regionprops(urinC15_bw,R,'PixelValues','MeanIntensity',...
    'MaxIntensity','MinIntensity');
G_stats = regionprops(urinC15_bw,G,'PixelValues','MeanIntensity',...
    'MaxIntensity','MinIntensity');
B_stats = regionprops(urinC15_bw,B,'PixelValues','MeanIntensity',...
    'MaxIntensity','MinIntensity');
 
R_pix_val = R_stats.PixelValues;
G_pix_val = G_stats.PixelValues;
B_pix_val = B_stats.PixelValues;
 
% figure,
% histogram(R_pix_val,256,'FaceColor','r','EdgeColor','r')
% set(gca,'XLim',[0 255])
% set(gca,'YLim',[0 15000])
% grid on
% title('Histogram of Red Channel')
%  
% figure,
% histogram(G_pix_val,256,'FaceColor','g','EdgeColor','g')
% set(gca,'XLim',[0 255])
% set(gca,'YLim',[0 15000])
% grid on
% title('Histogram of Green Channel')
%  
% figure,
% histogram(B_pix_val,256,'FaceColor','b','EdgeColor','b')
% set(gca,'XLim',[0 255])
% set(gca,'YLim',[0 15000])
% grid on
% title('Histogram of Blue Channel')

%setelah proses ekstraksi ciri RGB selanjutnya dikonversi kedalam
%segmentasi saturasi warna Hue, Saturation, Value (HSV) 
R_mean = R_stats.MeanIntensity;
G_mean = G_stats.MeanIntensity;
B_mean = B_stats.MeanIntensity;
 
R_max = R_stats.MaxIntensity;
G_max = G_stats.MaxIntensity;
B_max = B_stats.MaxIntensity;
 
R_min = R_stats.MinIntensity;
G_min = G_stats.MinIntensity;
B_min = B_stats.MinIntensity;
 
% % HSV features extraction
urinC15 = rgb2hsv(Img);
H = urinC15(:,:,1);
S = urinC15(:,:,2);
V = urinC15(:,:,3);
H(~urinC15_bw) = 0;
S(~urinC15_bw) = 0;
V(~urinC15_bw) = 0;
urinC15_hsv = cat(3,H,S,V);
figure, imshow(urinC15_hsv), title('the urinC15 only (HSV Color Space)');
 
H_stats = regionprops(urinC15_bw,H,'PixelValues','MeanIntensity',...
    'MaxIntensity','MinIntensity');
S_stats = regionprops(urinC15_bw,S,'PixelValues','MeanIntensity',...
    'MaxIntensity','MinIntensity');
V_stats = regionprops(urinC15_bw,V,'PixelValues','MeanIntensity',...
    'MaxIntensity','MinIntensity');
 
H_mean = H_stats.MeanIntensity;
S_mean = S_stats.MeanIntensity;
V_mean = V_stats.MeanIntensity;
 
H_max = H_stats.MaxIntensity;
S_max = S_stats.MaxIntensity;
V_max = V_stats.MaxIntensity;
 
H_min = H_stats.MinIntensity;
S_min = S_stats.MinIntensity;
V_min = V_stats.MinIntensity;
 
% Shape features extraction
reg_stats = regionprops(urinC15_bw,'Area','Perimeter','Eccentricity');
 
area = reg_stats.Area;
perimeter = reg_stats.Perimeter;
eccentricity = reg_stats.Eccentricity;

% menampilkan nilai dalam command window
disp('>>> Features Extracted <<<')
disp('urinC15.jpg')
disp('<1> Red Channel')
fprintf('Mean = %6.2f \n',R_mean);
fprintf('Max  = %6.0f \n',R_max);
fprintf('Min  = %6.0f \n',R_min);
 
disp('<2> Green Channel')
fprintf('Mean = %6.2f \n',G_mean);
fprintf('Max  = %6.0f \n',G_max);
fprintf('Min  = %6.0f \n',G_min);
 
disp('<3> Blue Channel')
fprintf('Mean = %6.2f \n',B_mean);
fprintf('Max  = %6.0f \n',B_max);
fprintf('Min  = %6.0f \n',B_min);
 
disp('<4> Hue Channel')
fprintf('Mean = %6.4f \n',H_mean);
fprintf('Max  = %6.4f \n',H_max);
fprintf('Min  = %6.4f \n',H_min);
 
disp('<5> Saturation Channel')
fprintf('Mean = %6.4f \n',S_mean);
fprintf('Max  = %6.4f \n',S_max);
fprintf('Min  = %6.4f \n',S_min);
 
disp('<6> Value Channel')
fprintf('Mean = %6.4f \n',V_mean);
fprintf('Max  = %6.4f \n',V_max);
fprintf('Min  = %6.4f \n',V_min);
 
disp('<7> Shape Measurements')
fprintf('Area          = %6.0f \n',area);
fprintf('Perimeter     = %6.2f \n',perimeter);
fprintf('Eccentricity  = %6.4f \n',eccentricity);