clear
close all
clc

%set parameter
jenis_layer = 'RGB'; %RGB, HSV, YCBCR, LAB
ukuran_gambar = 512; %256 512 1024
area_open = 1000; % 200 500 1000
jumlahLayerLVQ = 300; % 100 200 300 400 500
jumlahEpoch = 200; % 100 200 300 400 500

%inisiasi variabel
ciri_latih = [];
target = {};
path = uigetdir; %memilih folder
jenis = dir(path); %mengambil nilai path dari folder yg dipilih
nama = {};
group = [];
iter = 1;
classIter = 1;
fileAll = {};
fileName = {};
jumlahAll = [];
%perulangan berdasarkan banyak folder
for i=3:length(jenis)
    namafolder = jenis(i).name; %mengambil nama folder pada urutan ke - i
    tempatFile = [path '\' namafolder]; %menyatukan path dengan list folder (bohong atau jujur)
    filename = dir(tempatFile); %mengambil nilai path dari folder yg dipilih
    fileAll{classIter} = filename(3:end);
    jumlahAll = [jumlahAll length(filename)-2];
    classIter = classIter + 1;
    %perulangan berdasarkan banyaknya file mat pada folder kanal
    temp = [];
    for j=3:length(filename)
        namefile = filename(j).name; %memasukan nama file mat ke variabel
        fileName{iter} = namefile;
        iter = iter+1;
        bacaimage = [tempatFile '\' namefile]; %menyatukan file pathname dengan filename
        img = imread(bacaimage);
        img = imresize(img,[ukuran_gambar ukuran_gambar]);
        
        % konversi terlebih dahulu ke dalam L*a*b color space
        cform = makecform('srgb2lab');
        lab = applycform(img,cform);
        
        % proses segmentasi citra menggunakan k-mean clustering
        ab = double(lab(:,:,2:3));
        nrows = size(ab,1);
        ncols = size(ab,2);
        ab = reshape(ab,nrows*ncols,2);
        
        nColors = 3;
        [cluster_idx, cluster_center] = kmeans(ab,nColors,'distance','sqEuclidean','Replicates',3);
        pixel_labels = reshape(cluster_idx,nrows,ncols);
        RGB = label2rgb(pixel_labels);
        
        %menampilkan hasil segmentasi pada masing-masing cluster
        segmented_images = cell(1,3);
        rgb_label = repmat(pixel_labels,[1 1 3]);
        
        for k = 1:nColors
            color = img;
            color(rgb_label ~= k) = 0;
            segmented_images{k} = color;
        end
        
        %proses filling holes untuk menghilangkan noise
        % Urin segmentation
        area_cluster1 = sum(sum(pixel_labels==1));
        area_cluster2 = sum(sum(pixel_labels==2));
        area_cluster3 = sum(sum(pixel_labels==3));
        
        [~,cluster_all] = min([area_cluster1,area_cluster2,area_cluster3]);
        imgBw = (pixel_labels==cluster_all);
        imgBw = imfill(imgBw,'holes');
        imgBw = bwareaopen(imgBw,area_open);
        
        % Shape features extraction
        reg_stats = regionprops(imgBw,'Area','Perimeter','Eccentricity');
        
        area = reg_stats.Area;
        perimeter = reg_stats.Perimeter;
        eccentricity = reg_stats.Eccentricity;
        ciri_shape = [area perimeter eccentricity];
        
        if strcmp(jenis_layer,'RGB')
            temp = img;
            R = temp(:,:,1);
            G = temp(:,:,2);
            B = temp(:,:,3);
            R(~imgBw) = 0;
            G(~imgBw) = 0;
            B(~imgBw) = 0;
            urinCat = cat(3,R,G,B);
            
            % menampilkan histogram pada masing-masing kanal RGB
            % RGB Features Extraction
            R_stats = regionprops(imgBw,R,'PixelValues','MeanIntensity',...
                'MaxIntensity','MinIntensity');
            G_stats = regionprops(imgBw,G,'PixelValues','MeanIntensity',...
                'MaxIntensity','MinIntensity');
            B_stats = regionprops(imgBw,B,'PixelValues','MeanIntensity',...
                'MaxIntensity','MinIntensity');
            
            R_pix_val = R_stats.PixelValues;
            G_pix_val = G_stats.PixelValues;
            B_pix_val = B_stats.PixelValues;
            
            R_mean = R_stats.MeanIntensity;
            G_mean = G_stats.MeanIntensity;
            B_mean = B_stats.MeanIntensity;
            
            R_max = R_stats.MaxIntensity;
            G_max = G_stats.MaxIntensity;
            B_max = B_stats.MaxIntensity;
            
            R_min = R_stats.MinIntensity;
            G_min = G_stats.MinIntensity;
            B_min = B_stats.MinIntensity;
            
            ciri = [R_mean G_mean B_mean R_max G_max B_max R_min G_min B_min];
        elseif strcmp(jenis_layer,'HSV')
            % % HSV features extraction
            temp = rgb2hsv(img);
            H = temp(:,:,1);
            S = temp(:,:,2);
            V = temp(:,:,3);
            H(~imgBw) = 0;
            S(~imgBw) = 0;
            V(~imgBw) = 0;
            urinCat = cat(3,H,S,V);
            
            H_stats = regionprops(imgBw,H,'PixelValues','MeanIntensity',...
                'MaxIntensity','MinIntensity');
            S_stats = regionprops(imgBw,S,'PixelValues','MeanIntensity',...
                'MaxIntensity','MinIntensity');
            V_stats = regionprops(imgBw,V,'PixelValues','MeanIntensity',...
                'MaxIntensity','MinIntensity');
            
            H_mean = H_stats.MeanIntensity;
            S_mean = S_stats.MeanIntensity;
            V_mean = V_stats.MeanIntensity;
            
            H_max = H_stats.MaxIntensity;
            S_max = S_stats.MaxIntensity;
            V_max = V_stats.MaxIntensity;
            
            H_min = H_stats.MinIntensity;
            S_min = S_stats.MinIntensity;
            V_min = V_stats.MinIntensity;
            
            ciri = [H_mean S_mean V_mean H_max S_max V_max H_min S_min V_min];
        elseif strcmp(jenis_layer,'YCBCR')
            temp = rgb2ycbcr(img);
            Y = temp(:,:,1);
            CB = temp(:,:,2);
            CR = temp(:,:,3);
            Y(~imgBw) = 0;
            CB(~imgBw) = 0;
            CR(~imgBw) = 0;
            urinCat = cat(3,Y,CB,CR);
            
            Y_stats = regionprops(imgBw,Y,'PixelValues','MeanIntensity',...
                'MaxIntensity','MinIntensity');
            CB_stats = regionprops(imgBw,CB,'PixelValues','MeanIntensity',...
                'MaxIntensity','MinIntensity');
            CR_stats = regionprops(imgBw,CR,'PixelValues','MeanIntensity',...
                'MaxIntensity','MinIntensity');
            
            Y_mean = Y_stats.MeanIntensity;
            CB_mean = CB_stats.MeanIntensity;
            CR_mean = CR_stats.MeanIntensity;
            
            Y_max = Y_stats.MaxIntensity;
            CB_max = CB_stats.MaxIntensity;
            CR_max = CR_stats.MaxIntensity;
            
            Y_min = Y_stats.MinIntensity;
            CB_min = CB_stats.MinIntensity;
            CR_min = CR_stats.MinIntensity;
            ciri = [Y_mean CB_mean CR_mean Y_max CB_max CR_max Y_min CB_min CR_min];
        elseif strcmp(jenis_layer,'LAB')
            temp = lab;
            L = temp(:,:,1);
            A = temp(:,:,2);
            B = temp(:,:,3);
            L(~imgBw) = 0;
            A(~imgBw) = 0;
            B(~imgBw) = 0;
            urinCat = cat(3,L,A,B);
            
            L_stats = regionprops(imgBw,L,'PixelValues','MeanIntensity',...
                'MaxIntensity','MinIntensity');
            A_stats = regionprops(imgBw,A,'PixelValues','MeanIntensity',...
                'MaxIntensity','MinIntensity');
            B_stats = regionprops(imgBw,B,'PixelValues','MeanIntensity',...
                'MaxIntensity','MinIntensity');
            
            L_mean = L_stats.MeanIntensity;
            A_mean = A_stats.MeanIntensity;
            B_mean = B_stats.MeanIntensity;
            
            L_max = L_stats.MaxIntensity;
            A_max = A_stats.MaxIntensity;
            B_max = B_stats.MaxIntensity;
            
            L_min = L_stats.MinIntensity;
            A_min = A_stats.MinIntensity;
            B_min = B_stats.MinIntensity;
            ciri = [L_mean A_mean B_mean L_max A_max B_max L_min A_min B_min];
        end
        
        ciri_latih = [ciri_latih; ciri]; %menyimpan ciri latih untuk setiap file
        group = [group i-2]; %menyimpan nilai group target sesuai dengan masukan
    end
end
ciri_latih = double(ciri_latih);
ciri_latih = ciri_latih';
ciriMax = [];
for ii=1:size(ciri_latih,1)
    ciriMax = [ciriMax max(ciri_latih(ii,:))];
    ciri_latih(ii,:) = ciri_latih(ii,:)/ciriMax(ii);
end


target = ind2vec(group);

% net = train(net,ciri_latih);

% y = net(ciri_latih);

hiddenLayer= jumlahLayerLVQ;
net = lvqnet(hiddenLayer);
net.trainParam.epochs = jumlahEpoch;
net = train(net,ciri_latih,target);

save data_latih.mat ciri_latih target group net jenis_layer ukuran_gambar area_open ciriMax


% y2 = net(ciri_latih);
% perf2 = perform(net,y2,target);
% classes = vec2ind(y2);
% 
% jumlah = 0; %inisiasi jumlah akurasi
% 
% for l =1 : length(group) %iterasi sepanjang group
%     if group(l)==classes(l) %jika id_kelas_uji{l} sama dengan gr{l}
%         jumlah = jumlah + 1; %maka jumlah benar ditambah
%     end
% end
% akurasi = jumlah/length(group); %hitung akurasi

